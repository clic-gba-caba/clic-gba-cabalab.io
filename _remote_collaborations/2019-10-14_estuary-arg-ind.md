---
title: Estuary & Tidal Argentina-Indonesia
date: 2019-10-14
image: /images/estuary-arg-ind/estuary-arg-ind-1.jpg
---

En el marco de una charla sobre livecoding Rangga y Gigih, de Yogyakarta, Indonesia, colaboraron con munshkr e irisS, de Buenos Aires, Argentina, usando [Estuary](https://github.com/dktr0/estuary).
