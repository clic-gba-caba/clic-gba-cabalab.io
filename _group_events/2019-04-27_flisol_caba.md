---
title: FLISoL CABA
date: 2019-04-27
image: /images/flisol/irisS.jpg
---

Presentación en el marco del Festival Latinoamericano de Instalación de
Software Libre en la Ciudad Autónoma de Buenos Aires. Tocaron vlad, Matthi
Gatti e irisS.
